package ru.tsc.borisyuk.tm.api.repository;

import ru.tsc.borisyuk.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    List<Task> findAll();

}
