package ru.tsc.borisyuk.tm.api.controller;

import ru.tsc.borisyuk.tm.model.Project;

public interface IProjectController {

    void createProject();

    void showProjects();

    void showProject(Project project);

    void clearProjects();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

}
