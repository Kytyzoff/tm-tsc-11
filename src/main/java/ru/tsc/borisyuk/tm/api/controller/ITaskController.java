package ru.tsc.borisyuk.tm.api.controller;

import ru.tsc.borisyuk.tm.model.Task;

public interface ITaskController {

    void createTask();

    void showTasks();

    void showTask(Task task);

    void clearTasks();

    void showById();

    void showByIndex();

    void showByName();

    void removeById();

    void removeByIndex();

    void removeByName();

    void updateById();

    void updateByIndex();

}
