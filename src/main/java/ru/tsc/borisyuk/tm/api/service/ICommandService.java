package ru.tsc.borisyuk.tm.api.service;

import ru.tsc.borisyuk.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
